<?php 

function encabezado($titulo)
{
	$encabezado="<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Funciones y estiloss</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.5 -->
    <link rel='stylesheet' href='estilos/public/css/bootstrap.min.css'>
    <!-- Font Awesome -->
    <link rel='stylesheet' href='estilos/public/css/font-awesome.css'>
    <!-- Theme style -->
    <link rel='stylesheet' href='estilos/public/css/AdminLTE.min.css'>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel='stylesheet' href='estilos/public/css/_all-skins.min.css'>
    <link rel='apple-touch-icon' href='estilos/public/img/apple-touch-icon.png'>
    <link rel='shortcut icon' href='estilos/public/img/favicon.ico'>

    

  </head>
  <body class='hold-transition skin-blue-light sidebar-mini'>
    <div class='wrapper'>

      <header class='main-header'>

        <!-- Logo -->
        <a href='#' class='logo'>
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class='logo-mini'><b>IT</b>Funciones</span>
          <!-- logo for regular state and mobile devices -->
          <span class='logo-lg'><b>Funciones</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class='navbar navbar-static-top' role='navigation'>
          <!-- Sidebar toggle button-->
          <a href='#' class='sidebar-toggle' data-toggle='offcanvas' role='button'>
            <span class='sr-only'>Navegación</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class='navbar-custom-menu'>
            <ul class='nav navbar-nav'>
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class='dropdown user user-menu'>
                <a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                  <img src='estilos/public/dist/img/user2-160x160.jpg' class='user-image' alt='User Image'>
                  <span class='hidden-xs'></span>
                </a>
                <ul class='dropdown-menu'>
                  <!-- User image -->
                  <li class='user-header'>
                    <img src='estilos/public/dist/img/user2-160x160.jpg' class='img-circle' alt='User Image'>
                    <p>
                      Ariel Claros - Desarrollando Software para web
                      <small></small>
                    </p>
                  </li>
                  
                  <!-- Menu Footer-->
                  <li class='user-footer'>
                    
                    <div class='pull-right'>
                      <a href='#' class='btn btn-default btn-flat'>Cerrar</a>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>

        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class='main-sidebar'>
        <!-- sidebar: style can be found in sidebar.less -->
        <section class='sidebar'>       
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class='sidebar-menu'>
            <li class='header'></li>
            <li>
              <a href='#'>
                <i class='fa fa-tasks'></i> <span>Escritorio</span>
              </a>
            </li>            
            <li class='treeview'>
              <a href='#'>
                <i class='fa fa-laptop'></i>
                <span>Boton desplegable 1</span>
                <i class='fa fa-angle-left pull-right'></i>
              </a>
              <ul class='treeview-menu'>
                <li><a href='#'><i class='fa fa-circle-o'></i> Boton 1</a></li>
                <li><a href='#'><i class='fa fa-circle-o'></i> Boton 2</a></li>
              </ul>
            </li>
            
            <li class='treeview'>
              <a href='#'>
                <i class='fa fa-th'></i>
                <span>Boton desplegable 2</span>
                 <i class='fa fa-angle-left pull-right'></i>
              </a>
              <ul class='treeview-menu'>
                <li><a href='#'><i class='fa fa-circle-o'></i>Boton 3</a></li>
                <li><a href='#'><i class='fa fa-circle-o2'></i> Boton 4</a></li>
              </ul>
            </li>
            <li class='treeview'>
              <a href='#'>
                <i class='fa fa-shopping-cart'></i>
                <span>Boton desplegable 3</span>
                 <i class='fa fa-angle-left pull-right'></i>
              </a>
              <ul class='treeview-menu'>
                <li><a href='#'><i class='fa fa-circle-o'></i>Boton 5</a></li>
                <li><a href='#'><i class='fa fa-circle-o'></i> Boton 6</a></li>
              </ul>
            </li>                       
            <li class='treeview'>
              <a href='#'>
                <i class='fa fa-folder'></i> <span>Boton desplegable 4</span>
                <i class='fa fa-angle-left pull-right'></i>
              </a>
              <ul class='treeview-menu'>
                <li><a href='#'><i class='fa fa-circle-o'></i> Boton 7</a></li>
                <li><a href='#'><i class='fa fa-circle-o'></i> Boton 8</a></li>
                
              </ul>
            </li>
            <li class='treeview'>
              <a href='#'>
                <i class='fa fa-bar-chart'></i> <span>Boton desplegable 5</span>
                <i class='fa fa-angle-left pull-right'></i>
              </a>
              <ul class='treeview-menu'>
                <li><a href='#'><i class='fa fa-circle-o'></i>boton 9</a></li>                
              </ul>
            </li>
            <li class='treeview'>
              <a href='#'>
                <i class='fa fa-bar-chart'></i> <span>boton 10</span>
                <i class='fa fa-angle-left pull-right'></i>
              </a>
              <ul class='treeview-menu'>
                <li><a href='#'><i class='fa fa-circle-o'></i> boton 11</a></li>                
              </ul>
            </li>
            
            
                        
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
";

 echo $encabezado;
}


function piedepagina($footer)
{
	$pie="


    <footer class='main-footer'>
        <div class='pull-right hidden-xs'>
          <b>Version</b> 2.3.0
        </div>
        <strong>$footer<a href='https://www.facebook.com/arclark2294'>Ariel Claros</a>.</strong> All rights reserved.
    </footer>    
    <!-- jQuery -->
    <script src='estilos/public/js/jquery-3.1.1.min.js'></script>
    <!-- Bootstrap 3.3.5 -->
    <script src='estilos/public/js/bootstrap.min.js'></script>
    <!-- AdminLTE App -->
    <script src='estilos/public/js/app.min.js'></script>

   
  </body>
</html>


	 ";
 echo $pie;
}
 ?>


